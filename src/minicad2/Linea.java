package minicad2;


import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;

/**
 *
 * @author daira
 */
public class Linea extends Figura {
   Point punto1;
   Point punto2;

   Linea(Point _punto1, Point _punto2, Color _color) {
        punto1 = new Point(_punto1.x,_punto1.y);
        punto2 = new Point(_punto2.x,_punto2.y);        
        color  = _color;
   }
   
   Linea(String cadena) {
        punto1 = new Point();
        punto2 = new Point();        
        color  = Color.BLACK;
        
   }
   

   void dibujar(Graphics2D g2d){
        if (this.color!=null){
            g2d.setColor(color);
        }        
        g2d.drawLine(punto1.x,punto1.y,punto2.x,punto2.y);
   }
}